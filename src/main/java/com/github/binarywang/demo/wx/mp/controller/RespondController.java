package com.github.binarywang.demo.wx.mp.controller;

import com.github.binarywang.demo.wx.mp.service.WxMpTemplateMsgService;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author liangyongle
 * @date 2018/8/13 0013
 */
@RestController
@RequestMapping("respond")
public class RespondController {
    @Autowired
    private WxMpService wxMpService;

    @Resource
    private WxMpTemplateMsgService wxMpTemplateMsgService;
    /*
     * 获取用户信息
     **/
    @GetMapping("/memberCenter")
    public String getUserInfo(@RequestParam("openid") String
                                          openid){
        return openid;
    }

    /*
    *发送模板消息
    *
     */
    @GetMapping("/sendTemplate")
    public void sendTemplate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日");
        WxMpTemplateMessage templateMessage = WxMpTemplateMessage.builder().toUser("oEjrLwm0Vai8aurimDSKRSG6tSVY")
                .templateId("nxMezXONSZ0wQa_5WAAb-LoI2kOeYvrQEkcJgB9I0N4")
                .url("").build();
        templateMessage.addData(new WxMpTemplateData("first","你有新客户到访","#ff510"));
        templateMessage.addData(new WxMpTemplateData("keyword1","这是新的内容","#ff510"));
        templateMessage.addData(new WxMpTemplateData("keyword2",dateFormat.format(new Date()),"#ff510"));
        templateMessage.addData(new WxMpTemplateData("remark","备注","#ff510"));
        try{
            wxMpService.getTemplateMsgService().sendTemplateMsg(templateMessage);
        }catch(WxErrorException e){
            e.printStackTrace();
        }

    }


}
