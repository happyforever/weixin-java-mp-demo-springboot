package com.github.binarywang.demo.wx.mp.controller;

import com.github.binarywang.demo.wx.mp.config.WechatMpConfiguration;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liangyongle
 * @date 2018/8/13 0013
 */
@Controller
@RequestMapping("token")
@Slf4j
public class WechatTokenController {

    @Autowired
    private WxMpService wxMpService;

    @Autowired
    private WechatMpConfiguration wechatMpConfiguration;
    /*
    * 重定向地址
    * */
     @GetMapping("/authorize")
    public String authorize(@RequestParam("returnUrl") String returnUrl){
        //1.配置
         //2.调用方法
         String url = "http://lyl.natapp1.cc/token/userInfo";
         String redirectUrl = wxMpService.oauth2buildAuthorizationUrl(url, WxConsts.OAuth2Scope.SNSAPI_USERINFO, URLEncoder.encode(returnUrl));
         System.out.println("【微信网页授权】"+redirectUrl);
         return "redirect:"+redirectUrl;
     }


    /*
    * 获取微信回调信息
    * */
    @GetMapping("/userInfo")
    public String userInfo(@RequestParam("code") String code,
                           @RequestParam("state") String returnUrl){
        WxMpOAuth2AccessToken wxMpOAuth2AccessToken = new WxMpOAuth2AccessToken();
        try{
            wxMpOAuth2AccessToken =  wxMpService.oauth2getAccessToken(code);
        }catch(WxErrorException e){
            System.out.println("【微信网页获取】"+e.getMessage());
        }
        String openId = wxMpOAuth2AccessToken.getOpenId();
        String accessToken = wxMpOAuth2AccessToken.getAccessToken();
        WxMpUser wxMpUser = null;
        try {
            wxMpUser = wxMpService.oauth2getUserInfo(wxMpOAuth2AccessToken, null);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        System.out.println("[微信个人信息]"+wxMpUser);
        System.out.println("OPENID:"+openId);
        String nickname = wxMpUser.getNickname();
        Integer sex = wxMpUser.getSex();
        String headImgUrl = wxMpUser.getHeadImgUrl();
        String country = wxMpUser.getCountry();
        String city = wxMpUser.getCity();
        Map<String,Object> map = new HashMap<>();
        map.put("nickname",nickname);
        map.put("sex",sex);
        map.put("headImgUrl",headImgUrl);
        map.put("country",country);
        map.put("city",city);
        String redirectUrl = "http://lyl.natapp1.cc/respond/memberCenter";
        return "redirect:"+redirectUrl+"?openid="+openId;
    }





}
